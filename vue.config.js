// vue.config.js
const path = require('path')
module.exports = {
  publicPath: process.env.NODE_ENV === 'production' ? './' : '/',
  outputDir: "dist",
  assetsDir: "public",
  indexPath: 'index.html',
  configureWebpack: {
    resolve: { alias: { '@': path.join(__dirname, 'src') } },
  },

}
